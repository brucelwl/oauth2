package com.oauth2.client.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class Bootstrap implements WebApplicationInitializer{

	@Override
	public void onStartup(ServletContext container) throws ServletException {
		
		//设置静态资源位置
		container.getServletRegistration("default").addMapping("/resource/*");
		//
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(RootContextConfig.class);
		rootContext.refresh();
		
		container.addListener(new ContextLoaderListener(rootContext));
		
		/*************web servlet 上下文配置*****************/
		AnnotationConfigWebApplicationContext servletWebContext = new AnnotationConfigWebApplicationContext();
		servletWebContext.register(WebServletContextConfig.class);
		ServletRegistration.Dynamic dispatcher = container.addServlet("springWebDispatcher", new DispatcherServlet(servletWebContext));
		dispatcher.setLoadOnStartup(1);	
		dispatcher.addMapping("/");
	
	
	}
	

}
