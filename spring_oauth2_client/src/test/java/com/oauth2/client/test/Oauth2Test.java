package com.oauth2.client.test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.oauth2.client.util.HttpUtilslwl;

public class Oauth2Test {

	/**
	 * 密码模式
	 * 
	 * @throws IOException
	 */
	@Test
	public void passwordLoginTest() throws IOException {
		HttpUtilslwl httpUtilslwl = new HttpUtilslwl();

		String url2 = "http://localhost:8081/oauth2_provider/oauth/token";
		// username=demoUser1&password=123456&grant_type=password&client_id=demoApp&client_secret=demoAppSecret
		Map<String, String> params = new HashMap<>();
		params.put("username", "John");
		params.put("password", "123456");
		params.put("grant_type", "password");
		params.put("client_id", "demoApp");
		params.put("client_secret", "demoAppSecret");

		String httpResp2 = httpUtilslwl.httpClientdoPost(url2, params, null);
		TokenInfo tokenInfo = JSON.parseObject(httpResp2, TokenInfo.class);

		String url = "http://localhost:8081/oauth2_provider/rest/name";
		Map<String, String> pa = new HashMap<>();
		pa.put("access_token", tokenInfo.getAccessToken());
		String httpResp = httpUtilslwl.httpClientdoPost(url, pa, StandardCharsets.UTF_8.name());
		System.out.println(httpResp);

	}

	/**
	 * 客户端模式,适用于给第三方平台提供公共接口,不是针对个人提供接口
	 * 
	 * @throws IOException
	 */
	@Test
	public void clientLoginTest() throws IOException {
		HttpUtilslwl httpUtilslwl = new HttpUtilslwl();

		String url2 = "http://localhost:8081/oauth2_provider/oauth/token";
		// username=demoUser1&password=123456&grant_type=password&client_id=demoApp&client_secret=demoAppSecret
		Map<String, String> params = new HashMap<>();
		// params.put("username","John");
		// params.put("password","123456");
		params.put("grant_type", "client_credentials");
		params.put("client_id", "demoApp");
		params.put("client_secret", "demoAppSecret");

		String httpResp2 = httpUtilslwl.httpClientdoPost(url2, params, null);
		TokenInfo tokenInfo = JSON.parseObject(httpResp2, TokenInfo.class);

		String url = "http://localhost:8081/oauth2_provider/rest/name";
		Map<String, String> pa = new HashMap<>();
		pa.put("access_token", tokenInfo.getAccessToken());
		String httpResp = httpUtilslwl.httpClientdoPost(url, pa, StandardCharsets.UTF_8.name());
		System.out.println(httpResp);

	}

}
