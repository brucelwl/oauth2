//package com.oauth2.provider.config.component;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.oauth2.provider.ClientDetailsService;
//import org.springframework.security.oauth2.provider.ClientRegistrationException;
//import org.springframework.stereotype.Service;
//
//import com.oauth2.provider.dao.WebServiceClientRepository;
//
//@Service
//public class DefaultWebServiceClientService implements ClientDetailsService {
//
//	@Autowired
//	private WebServiceClientRepository repository;
//
//	@Override
//	public WebServiceClient loadClientByClientId(String clientId) throws ClientRegistrationException {
//
//		WebServiceClient client = repository.getByClientId(clientId);
//		if (client == null)
//			throw new ClientRegistrationException("Client not found");
//		return client;
//
//	}
//
//}
