/*package com.oauth2.provider.config.component;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

public class WebServiceClient implements ClientDetails, Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String clientId;
	private String clientSecret;
	private Set<String> scope;
	private Set<String> authorizedGrantTypes;
	private Set<String> registeredRedirectUri;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public Set<String> getScope() {
		return this.scope;
	}

	public void setScope(Set<String> scope) {
		this.scope = scope;
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return this.authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(Set<String> authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	@Override
	public Set<String> getRegisteredRedirectUri() {
		return this.registeredRedirectUri;
	}

	public void setRegisteredRedirectUri(Set<String> registeredRedirectUri) {
		this.registeredRedirectUri = registeredRedirectUri;
	}

	private static final Set<String> RESOURCE_IDS = new HashSet<>();
	private static final Set<GrantedAuthority> AUTHORITIES = new HashSet<>();
	static {
		RESOURCE_IDS.add("SUPPORT");
		AUTHORITIES.add(new SimpleGrantedAuthority("OAUTH_CLIENT"));
	}

	@Override
	public Set<String> getResourceIds() {
		return RESOURCE_IDS;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return AUTHORITIES;
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return 3600;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return -1;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		return null;
	}

	@Override
	public boolean isSecretRequired() {
		return true;
	}

	@Override
	public boolean isScoped() {
		return true;
	}

	@Override
	public boolean isAutoApprove(String scope) {
		return false;
	}
}
*/