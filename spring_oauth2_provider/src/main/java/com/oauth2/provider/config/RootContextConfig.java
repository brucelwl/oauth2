package com.oauth2.provider.config;

import java.util.logging.Logger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
@ComponentScan(basePackages = { "com.oauth2.provider.service","com.oauth2.provider.dao" },
excludeFilters = @ComponentScan.Filter({
		Controller.class, RestController.class }))
// 导入SpringSecurity配置
@Import({SecurityConfig.class,Oauth2ServerConfig.class})
//@ImportResource({ "classpath:securityConfiguration.xml" })
public class RootContextConfig {
	
	Logger log = Logger.getLogger("lwl");
	Logger schedulingLogger = Logger.getLogger("lwlTask");
	// 读取properties文件
	// @Autowired
	// private Environment environment;

	// json需要用到
	@Bean
	public ObjectMapper objectMapper() {
		/*
		 * System.out.println(environment.getProperty("location"));
		 * System.out.println(environment.getProperty("maxFileSize"));
		 * System.out.println(environment.getProperty("maxRequestSize"));
		 * System.out.println(environment.getProperty("fileSizeThreshold"));
		 */
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);

		return mapper;
	}

	// xml需要用到,一定要保证所有的xml注解可以被扫描到
	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan(new String[] { "com.oauth2.provider.entity" });
		return marshaller;
	}

}
