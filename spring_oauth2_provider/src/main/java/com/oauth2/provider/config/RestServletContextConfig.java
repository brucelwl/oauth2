package com.oauth2.provider.config;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Rest风格webservice 上下文配置
 * @author lwl 2018年4月17日 下午3:51:07
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
		"com.oauth2.provider.rest" }, useDefaultFilters = false, 
				includeFilters = @ComponentScan.Filter({
				RestController.class }))
public class RestServletContextConfig implements WebMvcConfigurer {
	
	@Autowired ObjectMapper objectMapper;
	@Autowired Marshaller marshaller;
	@Autowired Unmarshaller unmarshaller;
	//@Autowired SpringValidatorAdapter validator;

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new SourceHttpMessageConverter<>());
		Charset charsetUTF8 = StandardCharsets.UTF_8;
		// 字符串转换器
		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
		stringHttpMessageConverter.setSupportedMediaTypes(
				Arrays.asList(new MediaType("text", "html", charsetUTF8),
				// new MediaType("application", "json", charsetUTF8),
				new MediaType("application", "x-www-form-urlencoded", charsetUTF8),
				new MediaType("text", "plain", charsetUTF8)));
		//xml 转换器
		MarshallingHttpMessageConverter xmlConverter = new MarshallingHttpMessageConverter();
		xmlConverter.setSupportedMediaTypes(
				Arrays.asList(new MediaType("application", "xml"), new MediaType("text", "xml")));
		xmlConverter.setMarshaller(this.marshaller);
		xmlConverter.setUnmarshaller(this.unmarshaller);
		//json 转换器
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
		jsonConverter.setSupportedMediaTypes(
				Arrays.asList(new MediaType("application", "json"), new MediaType("text", "json")));
		jsonConverter.setObjectMapper(this.objectMapper);
		
		converters.add(stringHttpMessageConverter);
		converters.add(jsonConverter);
		converters.add(xmlConverter);
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false)
		          .favorParameter(false)
		          .ignoreAcceptHeader(false)
				  .defaultContentType(MediaType.APPLICATION_JSON);
	}

	
}
